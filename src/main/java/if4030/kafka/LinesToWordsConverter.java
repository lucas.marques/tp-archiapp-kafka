package if4030.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

/*
 * Création de topic: Ce composant tente de créer les topics lines-stream et words-stream s'ils n'existent pas.
 * Ce composant écoute le topic kafka lines-stream et convertit
 * son contenu initialement sous forme de lignes en mots.
 * Il extrait des lignes les mots séparés par des espaces, ne
 * compte pas les mots contenant des caractères spéciaux ( qui
 * ne sont pas dans le standard unicode) et conserve les mots
 * composés (ex: mot-valise).
 * Il publie ces mots sur le topic kafka words-stream
 */

public class LinesToWordsConverter {
    private static final String INPUT_TOPIC = "lines-stream";
    private static final String OUTPUT_TOPIC = "words-stream";

    static Properties getStreamsConfig(final String[] args) throws IOException {
        final Properties props = new Properties();
        if (args != null && args.length > 0) {
            try (final FileInputStream fis = new FileInputStream(args[0])) {
                props.load(fis);
            }
            if (args.length > 1) {
                System.out.println("Warning: Some command line arguments were ignored. This demo only accepts an optional configuration file.");
            }
        }
        props.putIfAbsent(StreamsConfig.APPLICATION_ID_CONFIG, "streams-lines-to-words");
        props.putIfAbsent(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.putIfAbsent(StreamsConfig.STATESTORE_CACHE_MAX_BYTES_CONFIG, 0);
        props.putIfAbsent(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.putIfAbsent(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        props.putIfAbsent(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    static void createLinesToWordsStream(final StreamsBuilder builder) {
        // Lecture du flux d'entrée
        final KStream<String, String> source = builder.stream(INPUT_TOPIC);
    
        // Filtre pour garder seulement les mots alphabétiques (caractères unicode) et les mots composés (mots séparés par "-")
        final Pattern pattern = Pattern.compile("\\p{L}+(-\\p{L}+)*"); 
        
        // Transformation des lignes lues sur le flux d'entrée en mots, et filtrage
        final KStream<String, String> words = source
            .flatMapValues(value -> Arrays.asList(value.split("\\s+")))
            .filter((key, word) -> pattern.matcher(word).matches());
        
        // Publication sur le flux de sortie
        words.to(OUTPUT_TOPIC, Produced.with(Serdes.String(), Serdes.String()));
    }

    public static void main(final String[] args) throws IOException {
        // On tente de créer les topics nécessaires au fonctionnement du composant
        KafkaTopicCreator.main(new String[] {"lines-stream"});
        KafkaTopicCreator.main(new String[] {"words-stream"});
        System.out.println("Running lines to words converter");
        final Properties props = getStreamsConfig(args);

        final StreamsBuilder builder = new StreamsBuilder();
        createLinesToWordsStream(builder);
        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (final Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
