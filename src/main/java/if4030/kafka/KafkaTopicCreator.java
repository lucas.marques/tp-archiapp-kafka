package if4030.kafka;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.Collections;
import java.util.Properties;

/*
 * Ce composant utilise l'API d'administration de Kafka pour créer un topic
 * Utilisation: main(["Nom du topic"])
 */

public class KafkaTopicCreator {
    public static void main(String[] args) throws IllegalArgumentException  {
        if (args.length != 1) {
            throw new IllegalArgumentException("Please specify the name of the topic to create");
        }
        String topicName = args[0];

        // Configuration des propriétés pour l'AdminClient
        Properties props = new Properties();
        props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        AdminClient adminClient = AdminClient.create(props);

        // Nom et nombre de partitions du nouveau topic
        int numPartitions = 1;

        // Vérifie si le topic existe déjà
        try {
            adminClient.describeTopics(Collections.singleton(topicName)).allTopicNames().get();
            System.out.println("Topic "+ topicName + " already exists");
            adminClient.close();
            return;
        } catch (Exception e) {
            // Le topic n'existe pas encore, on continue la création
        }

        // Création du nouveau topic
        NewTopic newTopic = new NewTopic(topicName, numPartitions, (short) 1); // le facteur de réplication est fixé à 1 ici
        try {
            adminClient.createTopics(Collections.singleton(newTopic)).all().get();
            System.out.println("Topic "+ topicName +" created successfully");
        } catch (Exception e) {
            System.out.println("Failed to create topic: " + e.getMessage());
        } finally {
            adminClient.close();
        }
    }
}