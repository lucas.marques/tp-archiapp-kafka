package if4030.kafka;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Properties;
import java.util.Set;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

/*  
 * Création de topic: Ce composant tente de créer les topics tagged-words-stream et command-topic s'ils n'existent pas.
 * Ce composant écoute le topic kafka tagged-words-stream et compte le nombre d'occurence
 * de chaque lemme pour chaque catégorie.
 * Les catégories peu utiles sont listées dans la variables FILTERED_CATEGORIES et son filtrées
 * Le composant écoute aussi le topic command-topic qui permet de le commander.
 * Référence des commandes:
 * "END": Le composant affiche les 20 (valeur par défaut) mots les plus fréquents pour chaque catégorie et s'arrête.
 * "DISPLAY <Int>": Permet de modifier le nombre de mots affichés pour chaque catégorie via la commande END. <Int>
 *                  doit être compris entre 1 et 50.
 * "RESET": Permet de remettre à zéro le compteur.
 */


public class WordCounter {
    private static final String INPUT_TOPIC = "tagged-words-stream";
    private static final String COMMAND_TOPIC = "command-topic";
    // Ces catégories correspondent à des mots peu utiles pour une analyse, les mots correspondants sont filtrés
    private static Set<String> FILTERED_CATEGORIES = new HashSet<>(Arrays.asList(
        "ADJ:dem",
        "ADJ:ind",
        "ADJ:int",
        "ADJ:num",
        "ADJ:pos",
        "ART:inf", 
        "ART:def", 
        "CON",
        "LIA",
        "PRE",
        "PRO:dem", 
        "PRO:ind", 
        "PRO:int",
        "PRO:per",
        "PRO:rel",
        null
    ));
    // Il est possible de filtrer des mots grâce à la variable suivante. Par défaut, on filtre les mots "vides"
    private static final Set<String> FILTERED_WORDS = new HashSet<>(Arrays.asList("", null));
    // Dictionnaire et files de priorité pour le comptage des mots par catégorie
    private static Map<String, Map<String, Integer>> categoryWordCountMap = new HashMap<>();
    private static Map<String, PriorityQueue<Map.Entry<String, Integer>>> categoryPQMap= new HashMap<>();
    private static Integer maxOutputRecordsByCategory = 20;

    private static void countWord(String category, String word) {
        // Récupération du compte du mot correspondant à sa catégorie et incrémentation
        Map<String, Integer> wordCountMap = categoryWordCountMap.getOrDefault(category, new HashMap<>());
        wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);
        categoryWordCountMap.put(category, wordCountMap);


        // Récupération de la file de priorité correspondant à la catagorie du mot compté
        PriorityQueue<Map.Entry<String, Integer>> pq = categoryPQMap.getOrDefault(category, new PriorityQueue<>((a, b) -> b.getValue() - a.getValue()));
        categoryPQMap.put(category, pq);
        // Si le mot était déjà dans la file, on retire l'ancien compte
        pq.removeIf(entry -> entry.getKey().equals(word));
        // Ajout à la file de priorité du compte incrémenté
        pq.offer(new AbstractMap.SimpleEntry<>(word, wordCountMap.get(word)));
    }
    public static void main(String[] args) {
        // On tente de créer les topics nécessaires au fonctionnement du composant
        KafkaTopicCreator.main(new String[] {"tagged-words-stream"});
        KafkaTopicCreator.main(new String[] {"command-topic"});
        // Initialisation du consommateur
        System.out.println("Running consumer");
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "tagged-words-consumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        // Si on spécifie une catégorie en argument, on ne prend compte que celle-ci
        String SELECTED_CATEGORY = null;
        if (args.length == 1) {
            SELECTED_CATEGORY = args[0];
            System.out.println("Counting words with category " + SELECTED_CATEGORY);
        }

        KafkaConsumer<String, String> wordsConsumer = new KafkaConsumer<>(props);
        wordsConsumer.subscribe(Collections.singletonList(INPUT_TOPIC));
        KafkaConsumer<String, String> commandConsumer = new KafkaConsumer<>(props);
        commandConsumer.subscribe(Collections.singletonList(COMMAND_TOPIC));
        
        boolean running = true;

        while (running) {
            // Consommation des messages sur le topic d'entrée
            ConsumerRecords<String, String> records = wordsConsumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {
                String category = record.key();
                String word = record.value();
                // Si pas de catégorie spécifiée en argument, les mots sont filtrés selon leur catégorie
                if ((SELECTED_CATEGORY == null && !FILTERED_CATEGORIES.contains(category) && !FILTERED_WORDS.contains(word))) {
                    countWord(category, word);
                } else if (SELECTED_CATEGORY != null && category != null && SELECTED_CATEGORY.equals(category)) {
                    countWord(category, word);
                }
            }
            
            ConsumerRecords<String, String> commandRecords = commandConsumer.poll(Duration.ofMillis(0));
            for (ConsumerRecord<String, String> commandRecord : commandRecords) {
                String command = commandRecord.value().trim();
                // Commande "END"
                if ("END".equals(command)) {
                    System.out.println("Received stop command, printing the top " + Integer.toString(maxOutputRecordsByCategory) + " words for each category:");
                    for (String category : categoryPQMap.keySet()) {
                        System.out.println("Category: " + category);
                        PriorityQueue<Map.Entry<String, Integer>> pq = categoryPQMap.get(category);
                        for (int i = 0; i < maxOutputRecordsByCategory && !pq.isEmpty(); i++) {
                            Map.Entry<String, Integer> entry = pq.poll();
                            System.out.println(entry.getKey() + " : " + entry.getValue());
                        }
                        System.out.println();
                    }
                    running = false;
                }

                // Commande "RESET"
                if ("RESET".equals(command)) {
                    System.out.println("Resetting the counter");
                    categoryWordCountMap = new HashMap<>();
                    categoryPQMap= new HashMap<>();
                }

                // Command "DISPLAY X" (Range 1-50)
                String[] parts = command.split("\\s+");
                if (parts.length == 2 && parts[0].equals("DISPLAY")) {
                    try {
                        int num = Integer.parseInt(parts[1]);
                        if (num >= 1 && num <= 50) {
                            System.out.println("Max number records to show is set to " + num);
                            maxOutputRecordsByCategory = num;
                        } else {
                            System.out.println("Your argument for the display command is out of range");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Invalid input for display command");
                    }
                }
            }
        }
        wordsConsumer.close();
        commandConsumer.close();
    }
}