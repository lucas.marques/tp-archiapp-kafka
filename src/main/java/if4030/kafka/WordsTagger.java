package if4030.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/*
 * Création de topic: Ce composant tente de créer les topics tagged-words-stream et words-stream s'ils n'existent pas.
 * Ce composant écoute le topic kafka words-stream et utilise la base de données
 * Lexique3 pour en extraire la catégorie et les remplacer par leur lemme.
 * Pour chaque mot, il publie sur le topic kafka tagged-words-stream un couple
 * clé/valeur correspondant au couple catégorie/lemme du mot.
 * Si le mot ne correspond à aucune catégorie, il publie null/null.
 * Pour lancer ce composant, il est nécéssaire de se placer dans un répertoire
 * contenant le fichier Lexique383.tsv, téléchargeable ici
 * http://www.lexique.org/databases/Lexique383/
 */

public class WordsTagger {
    private static final String INPUT_TOPIC = "words-stream";
    private static final String OUTPUT_TOPIC = "tagged-words-stream";

    static Properties getStreamsConfig(final String[] args) throws IOException {
        final Properties props = new Properties();
        if (args != null && args.length > 0) {
            try (final FileInputStream fis = new FileInputStream(args[0])) {
                props.load(fis);
            }
            if (args.length > 1) {
                System.out.println("Warning: Some command line arguments were ignored. This demo only accepts an optional configuration file.");
            }
        }
        props.putIfAbsent(StreamsConfig.APPLICATION_ID_CONFIG, "streams-words-to-tagged");
        props.putIfAbsent(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.putIfAbsent(StreamsConfig.STATESTORE_CACHE_MAX_BYTES_CONFIG, 0);
        props.putIfAbsent(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.putIfAbsent(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        props.putIfAbsent(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    static void createTaggedWordsStream(final StreamsBuilder builder, Map<String, String[]> dictionary) throws IOException {
        // Lecture du flux d'entrée
        final KStream<String, String> source = builder.stream(INPUT_TOPIC);
        
        // Utilisation du dictionnaire de Lexique pour récupérer le lemme et la catégorie de chaque mot du flux d'entrée
        final KStream<String, String> lemmas = source.map((key, word) -> {
            String[] lemmaAndCategory = dictionary.get(word);
            if (lemmaAndCategory != null) {
                return new KeyValue<String, String>(lemmaAndCategory[1], lemmaAndCategory[0]);
            }
            return new KeyValue<String, String>(null, null);
        });

        // Publication du couple catégorie/lemme de chaque mot sur le flux de sortie
        lemmas.to(OUTPUT_TOPIC, Produced.with(Serdes.String(), Serdes.String()));
    }

    public static void main(final String[] args) throws IOException {
        // On tente de créer les topics nécessaires au fonctionnement du composant
        KafkaTopicCreator.main(new String[] {"words-stream"});
        KafkaTopicCreator.main(new String[] {"tagged-words-stream"});
        System.out.println("Running word tagger");
        
        // Lecture du Lexique et stockage dans un dictionnaire
        final Map<String, String[]> dictionary = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader("./Lexique383.tsv"));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] cols = line.split("\t");
            dictionary.put(cols[0], new String[]{cols[2], cols[3]});
        }
        reader.close();
        final Properties props = getStreamsConfig(args);

        final StreamsBuilder builder = new StreamsBuilder();
        createTaggedWordsStream(builder, dictionary);
        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (final Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
