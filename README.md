## TP Kafka - Architecture Applicatives
https://wdi.centralesupelec.fr/3IF4030/TPKafka

# Prérequis
Utiliser l'image Docker trouvable ici: https://gitlab-research.centralesupelec.fr/my-docker-images/docker-openvscode-server-kafka

# Build
Compiler le code avec 
```
mvn package
```

# Run
Se placer dans le répertoire racine du projet. S'assurer qu'il existe le fichier ./Lexique383.tsv

Créer un espace de stockage pour le serveur Kafka
```
kafka-storage.sh format -t `kafka-storage.sh random-uuid` -c /opt/kafka/config/kraft/server.properties
```

Lancement du serveur Kafka
```
kafka-server-start.sh /opt/kafka/config/kraft/server.properties
```

Lancer chaque composant dans 3 terminaux différents avec:
Ces composants crééent automatiquement les topics kafka nécessaires grâce à la classe KafkaTopicCreator qui se base sur l'API d'administration Kafka.
```
java -cp target/tp-kafka-0.0.1-SNAPSHOT.jar if4030.kafka.LinesToWordsConverter
```
```
java -cp target/tp-kafka-0.0.1-SNAPSHOT.jar if4030.kafka.WordsTagger
```
```
java -cp target/tp-kafka-0.0.1-SNAPSHOT.jar if4030.kafka.WordCounter
```
 
Ouvrir un producteur ou consommateur (spécifier le topic: "lines-stream", "words-stream", "tagged-words-stream" ou "command-topic")

```
kafka-console-producer.sh --bootstrap-server localhost:9092 --topic <TOPIC>
```
```
kafka-console-consumer.sh --bootstrap-server localhost:9092 \
        --topic <TOPIC> --from-beginning \
        --formatter kafka.tools.DefaultMessageFormatter --property print.key=true \
        --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
        --property value.deserializer=org.apache.kafka.common.serialization.StringDeserializer
```

Envoyer le contenu du fichier sur le producteur "lines-stream"
```
cat Les_Fourberies_de_Scapin.txt | kafka-console-producer.sh --bootstrap-server localhost:9092 --topic lines-stream
```

Commandes disponibles sur "command-topic" pour commander WordCounter

 * "END": Le composant affiche les 20 (valeur par défaut) mots les plus fréquents pour chaque catégorie et s'arrête.
 * "DISPLAY {Int}": Permet de modifier le nombre de mots affichés pour chaque catégorie via la commande END. {Int} doit être compris entre 1 et 50.
 * "RESET": Permet de remettre à zéro le compteur.